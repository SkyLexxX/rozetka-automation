from selenium import webdriver

CHROME_DRIVER = "chrome"
FIREFOX_DRIVER = "firefox"


class DriverFactory:
    @staticmethod
    def get_driver(driver_name):
        driver_name = driver_name if driver_name else CHROME_DRIVER
        if driver_name.lower() == CHROME_DRIVER:
            web_driver = webdriver.Chrome()
        elif driver_name.lower() == FIREFOX_DRIVER:
            web_driver = webdriver.Firefox()
        else:
            raise ValueError("Undefined web driver name")
        return web_driver
