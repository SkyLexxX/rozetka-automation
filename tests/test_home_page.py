import pytest
from faker import Faker

from config.settings import Settings
from pages.checkout import CheckoutPage
from pages.home import HomePage
from pages.item_detail import ItemDetailPage
from pages.search_results import SearchResultsPage


class TestHomePage:
    @pytest.mark.parametrize("browser", ["chrome"], indirect=True)
    @pytest.mark.parametrize("search_word", ["logitech g pro"])
    def test_buy_item_by_search_keyword(self, browser, search_word):
        home_page = HomePage(browser)
        home_page.enter_word(search_word)
        home_page.click_search()

        search_results_page = SearchResultsPage(browser)
        assert search_results_page.is_relevant_catalog_heading(search_word)

        results = search_results_page.get_all_items_titles_on_page()
        assert search_results_page.is_relevant_results(search_word, results)

        search_results_item_price = search_results_page.view_random_item_detail()
        detail_page = ItemDetailPage(browser)
        assert detail_page.is_detail_price_equal_search_results_price(
            search_results_item_price
        )

        detail_page.add_item_to_card()
        detail_page.submit_purchase()

        checkout_page = CheckoutPage(browser)
        faker = Faker(Settings.FAKER_LOCALE)
        checkout_page.fill_my_contact_data(
            faker.first_name(), faker.last_name(), faker.phone_number()
        )
        checkout_page.fill_recipient_contact_data(
            faker.first_name(),
            faker.last_name(),
            faker.last_name(),
            faker.phone_number(),
        )

        assert checkout_page.is_card_sum_the_same_on_checkout(search_results_item_price)
