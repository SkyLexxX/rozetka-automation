import pytest

from utils.driverfactory import DriverFactory


@pytest.fixture(scope="session", params=[None])
def browser(request):
    web_driver = DriverFactory.get_driver(request.param)
    yield web_driver
    web_driver.close()
