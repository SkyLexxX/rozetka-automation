from selenium.webdriver.common.by import By


class DetailLocators:
    LOCATOR_ITEM_PRICE = (By.CLASS_NAME, "product-prices__big")
    LOCATOR_BUY_BUTTON = (By.CLASS_NAME, "buy-button")
    LOCATOR_SUBMIT_PURCHASE = (By.CLASS_NAME, "cart-receipt__submit")
    LOCATOR_CLOSE_CARD = (By.CLASS_NAME, "modal__close")
