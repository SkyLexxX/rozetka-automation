from selenium.webdriver.common.by import By


class SearchResultsLocators:
    LOCATOR_CATALOG_HEADING = (By.CLASS_NAME, "catalog-heading")
    LOCATOR_GOODS_TILE = (By.CLASS_NAME, "catalog-grid__cell")
    LOCATOR_GOODS_TITLE = (By.CLASS_NAME, "goods-tile__heading")
    LOCATOR_ITEM_PRICE = (By.CLASS_NAME, "goods-tile__price-value")
