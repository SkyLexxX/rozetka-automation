from selenium.webdriver.common.by import By


class SearchLocators:
    LOCATOR_SEARCH_FIELD = (By.CLASS_NAME, "search-form__input")
    LOCATOR_SEARCH_BUTTON = (By.CLASS_NAME, "search-form__submit")
