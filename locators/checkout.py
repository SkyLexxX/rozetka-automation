from selenium.webdriver.common.by import By


class CheckoutLocators:
    FIRST_CHECKOUT_CONTACT_FORM = (
        "/html/body/app-root/div/div/rz-checkout-main/section/rz-checkout-orders/"
        "rz-checkout-orders-content/div/form/div/main/rz-checkout-contact-info/fieldset/"
    )

    LOCATOR_SURNAME = (
        By.XPATH,
        f"{FIRST_CHECKOUT_CONTACT_FORM}div[2]/div[1]/div[1]/input",
    )
    LOCATOR_NAME = (
        By.XPATH,
        f"{FIRST_CHECKOUT_CONTACT_FORM}div[2]/div[1]/div[2]/input",
    )
    LOCATOR_PHONE = (
        By.XPATH,
        f"{FIRST_CHECKOUT_CONTACT_FORM}div[2]/div[2]/div[1]/input",
    )

    LOCATOR_RECIPIENT_SURNAME = (By.ID, "recipientSurname")
    LOCATOR_RECIPIENT_NAME = (By.ID, "recipientName")
    LOCATOR_RECIPIENT_PATRONYMIC = (By.ID, "recipientPatronymic")
    LOCATOR_RECIPIENT_PHONE = (By.ID, "recipientTel")

    LOCATOR_CHECKOUT_TOTAL = (By.CLASS_NAME, "checkout-total__value")
