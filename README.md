# rozetka-automation

This is a simple autmations tests for Rozetka site

# Setting Up Development Environment

Create a virtualenv:

```
$ python3 -m venv <virtual env path>
```

Activate the virtualenv you have just created:

```
$ source <virtual env path>/bin/activate
```

Install development requirements:

```
$ pip install -r requirements.txt
$ git init # A git repo is required for pre-commit to install
$ pre-commit install
```

Run tests:

```
pytest
```
