import random
from typing import List

from locators import SearchResultsLocators
from pages.base import BasePage


class SearchResultsPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def get_all_items_titles_on_page(self) -> List[str]:
        goods = self.find_elements(SearchResultsLocators.LOCATOR_GOODS_TITLE)
        return [x.text for x in goods if len(x.text)]

    def view_random_item_detail(self) -> int:
        items = self.find_elements(SearchResultsLocators.LOCATOR_GOODS_TILE)
        chosen_item = items[random.randrange(0, len(items))]
        by, value = SearchResultsLocators.LOCATOR_ITEM_PRICE
        item_price = chosen_item.find_element(by, value)
        chosen_item.click()
        return int(item_price.text.replace(" ", ""))

    def is_relevant_catalog_heading(self, search_word) -> bool:
        catalog_heading = self.find_element(
            SearchResultsLocators.LOCATOR_CATALOG_HEADING
        )
        return search_word in catalog_heading.text

    @staticmethod
    def is_relevant_results(search_word, results) -> List[bool]:
        return [
            any(keyword in result.lower() for keyword in search_word.split())
            for result in results
        ]
