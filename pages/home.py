from config.settings import Settings
from locators import SearchLocators
from pages.base import BasePage


class HomePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(Settings.BASE_URL)

    def enter_word(self, word):
        return self.enter_data(word, SearchLocators.LOCATOR_SEARCH_FIELD)

    def click_search(self):
        search_button = self.find_element(SearchLocators.LOCATOR_SEARCH_BUTTON)
        search_button.click()
        return search_button
