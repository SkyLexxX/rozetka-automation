from locators import CheckoutLocators
from pages.base import BasePage


class CheckoutPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def fill_my_contact_data(self, surname, name, phone_number) -> None:
        self.enter_surname(surname)
        self.enter_name(name)
        self.enter_phone(phone_number)

    def fill_recipient_contact_data(
        self, surname, name, patronymic, phone_number
    ) -> None:
        self.enter_recipient_surname(surname)
        self.enter_recipient_name(name)
        self.enter_recipient_patronymic(patronymic)
        self.enter_recipient_phone(phone_number)

    def enter_surname(self, surname):
        return self.enter_data(surname, CheckoutLocators.LOCATOR_SURNAME)

    def enter_name(self, name):
        return self.enter_data(name, CheckoutLocators.LOCATOR_NAME)

    def enter_phone(self, phone_number):
        return self.enter_data(phone_number, CheckoutLocators.LOCATOR_PHONE)

    def enter_recipient_surname(self, surname):
        return self.enter_data(surname, CheckoutLocators.LOCATOR_RECIPIENT_SURNAME)

    def enter_recipient_name(self, name):
        return self.enter_data(name, CheckoutLocators.LOCATOR_RECIPIENT_NAME)

    def enter_recipient_patronymic(self, name):
        return self.enter_data(name, CheckoutLocators.LOCATOR_RECIPIENT_PATRONYMIC)

    def enter_recipient_phone(self, phone_number):
        return self.enter_data(phone_number, CheckoutLocators.LOCATOR_RECIPIENT_PHONE)

    def is_card_sum_the_same_on_checkout(self, card_sum) -> bool:
        checkout_price = self.find_element(CheckoutLocators.LOCATOR_CHECKOUT_TOTAL).text
        return int(card_sum) == int("".join(checkout_price[:-1].split()))
