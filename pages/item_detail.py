from locators.detail import DetailLocators
from pages.base import BasePage


class ItemDetailPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def add_item_to_card(self):
        buy_button = self.find_element(DetailLocators.LOCATOR_BUY_BUTTON)
        buy_button.click()
        return buy_button

    def submit_purchase(self):
        submit_button = self.find_element(DetailLocators.LOCATOR_SUBMIT_PURCHASE)
        submit_button.click()
        return submit_button

    def is_detail_price_equal_search_results_price(
        self, search_results_price: int
    ) -> bool:
        item_detail_price = self.find_element(DetailLocators.LOCATOR_ITEM_PRICE).text
        return search_results_price == int("".join(item_detail_price[:-1].split()))
